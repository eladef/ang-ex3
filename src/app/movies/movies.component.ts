import { Component, OnInit } from '@angular/core';
import { MovieComponent } from '../movie/movie.component';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {MatSelectModule} from '@angular/material/select';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies=[];
  studios=[];
  name = "no movie";
  studio="";
  displayedColumns: string[] = ['id', 'title', 'studio', 'income', 'delete'];

           


           toDelete(element)
           {
         
             let start, end = this.movies;
             let id = element.id;
             let i = 0;
             let deleted = [];
               
             for(let movie of this.movies)
             {
               deleted[i] = movie.id;
               i++;
             }
             
             start = this.movies.slice(0,deleted.indexOf(id));
             end = this.movies.slice(deleted.indexOf(id)+1, this.movies.length+1);
             this.movies = start;
             this.movies = this.movies.concat(end);   
             this.name = element.title;
         
           }


          //----------------ex 3 only--------------------------------- 

         // hideHim(mov) {
         //   this.name = mov.title;
         //   document.getElementById(mov.id).style.display = 'none';
         // }


          // movies = [
          //   {"id": 1, "title": "Spider-Man: Into The Spider-Verse", "Studio": "Sony", "weekend_income": "35,400,000$"},
          //   {"id": 2, "title": "The Mule", "Studio": "WB", "weekend_income": "17,210,000$"},
          //   {"id": 3, "title": "Dr. Seuss' The Grinch(2018)", "Studio": "Uni.", "weekend_income": "11,580,00$"},
          //    {"id": 4, "title": "Ralph Breaks the Internet", "Studio": "BV", "weekend_income": "9,589,000$"},
          //   {"id": 5, "title": "Mortal Engines", "Studio": "Uni.", "weekend_income": "7,501,000$"}
          //  ]


  constructor(private db:AngularFireDatabase) { }



  ngOnInit() {
      this.db.list('/movies').snapshotChanges().subscribe(
        movies => {
          this.movies=[];
          this.studios = ['all'];
          movies.forEach(
            movie=>{
            let y = movie.payload.toJSON();
            this.movies.push(y);  
            let stu = y['studio'];
            if (this.studios.indexOf(stu) == -1) {
              this.studios.push(y['studio']);
            }
            }
          )
        }
      )

    }

    toFilter() {
      let id = 1;
      this.db.list('/movies').snapshotChanges().subscribe(
        movies => {
          this.movies = [];
          movies.forEach(
            movie => {
              let y = movie.payload.toJSON();
              if (this.studio == 'all') {
                this.movies.push(y);
              }
              else if (y['studio'] == this.studio) {
                y['id'] = id;
                id++;
                this.movies.push(y);
              }
            }
          )
        }
      )
    }

  



}
