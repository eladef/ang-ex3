// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyDheSpvgkLW17OKT6mAcH-rj9EKgR_wLFc",
  authDomain: "ex-3-and-4-2311b.firebaseapp.com",
  databaseURL: "https://ex-3-and-4-2311b.firebaseio.com",
  projectId: "ex-3-and-4-2311b",
  storageBucket: "ex-3-and-4-2311b.appspot.com",
  messagingSenderId: "453677363715"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
